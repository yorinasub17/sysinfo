#!/bin/sh

BASEDIRNAME=${PWD##*/}
GOOS=${GOOS:-linux}
GOARCH=${GOARCH:-amd64}
BIN_TARGET=${BIN_TARGET:-build/main}


docker run --rm -it -v `pwd`:/go/src/$BASEDIRNAME -w /go/src/$BASEDIRNAME -e GOOS=$GOOS -e GOARCH=$GOARCH -e BIN_TARGET=$BIN_TARGET golang-dep:1.10 $@
