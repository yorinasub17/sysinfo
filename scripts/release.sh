#!/bin/sh

mkdir -p build

GOOS=linux GOARCH=amd64 BIN_TARGET=build/sysinfo-linux-x86_64 ./scripts/buildwrapper.sh ./scripts/build.sh
GOOS=linux GOARCH=arm BIN_TARGET=build/sysinfo-linux-arm ./scripts/buildwrapper.sh ./scripts/build.sh
GOOS=linux GOARCH=arm64 BIN_TARGET=build/sysinfo-linux-arm64 ./scripts/buildwrapper.sh ./scripts/build.sh
