#!/bin/sh

BIN_TARGET=${BIN_TARGET:-build/main}

dep ensure
go build -o $BIN_TARGET
