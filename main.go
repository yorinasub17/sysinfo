package main

import (
    "syscall"
    "fmt"
    "io/ioutil"
    "log"
    "strings"
    "strconv"
    "math"
    "runtime"
    "time"
    "github.com/daviddengcn/go-colortext"
)

func GetUpTime() (int64, int64, error) {
    b, err := ioutil.ReadFile("/proc/uptime")
    if err != nil {
        return -1, -1, err
    }
    raw := string(b)
    data := strings.Fields(raw)

    secondsUp, err := strconv.ParseFloat(data[0], 64)
    if err != nil {
        return -1, -1, err
    }

    secondsIdle, err := strconv.ParseFloat(data[1], 64)
    if err != nil {
        return -1, -1, err
    }

    return int64(secondsUp), int64(secondsIdle), err
}

func GetLoadAvg() (float64, float64, float64, error) {
    b, err := ioutil.ReadFile("/proc/loadavg")
    if err != nil {
        return -1, -1, -1, err
    }
    raw := string(b)
    data := strings.Fields(raw)

    oneMinuteLoad, err := strconv.ParseFloat(data[0], 32)
    if err != nil {
        return -1, -1, -1, err
    }

    fiveMinuteLoad, err := strconv.ParseFloat(data[1], 32)
    if err != nil {
        return -1, -1, -1, err
    }

    fifteenMinuteLoad, err := strconv.ParseFloat(data[2], 32)
    if err != nil {
        return -1, -1, -1, err
    }

    return float64(oneMinuteLoad), float64(fiveMinuteLoad), float64(fifteenMinuteLoad), nil
}

func GetCPUUsage() (float32, error) {
    b, err := ioutil.ReadFile("/proc/stat")
    if err != nil {
        return -1, err
    }
    raw := string(b)
    allInfo := strings.Split(raw, "\n")
    cpuData := strings.Fields(allInfo[0])

    cpuUser, err := strconv.ParseInt(cpuData[1], 10, 64)
    if err != nil {
        return -1, err
    }

    cpuSystem, err := strconv.ParseInt(cpuData[3], 10, 64)
    if err != nil {
        return -1, err
    }

    cpuIdle, err := strconv.ParseInt(cpuData[4], 10, 64)
    if err != nil {
        return -1, err
    }

    cpuUsage := float32(float64(cpuUser + cpuSystem)*100.0 / float64(cpuUser + cpuSystem + cpuIdle))
    return cpuUsage, nil
}

func GetCPUTemp() (float32, error) {
    b, err := ioutil.ReadFile("/sys/class/thermal/thermal_zone0/temp")
    if err != nil {
        return -1, err
    }
    rawData := strings.TrimSpace(string(b))
    temp, err := strconv.Atoi(rawData)
    if err != nil {
        return -1, err
    }
    // Unit is milli-degrees C
    return float32(temp) / 1000.0, nil
}

func GetMemInfo() (int32, int32, int32, int32, error) {
    b, err := ioutil.ReadFile("/proc/meminfo")
    if err != nil {
        return -1, -1, -1, -1, err
    }
    raw := string(b)
    allInfo := strings.Split(raw, "\n")
    memInfo := make(map[string]int32)
    for i := 0; i < len(allInfo); i++ {
        data := strings.Fields(allInfo[i])
        if len(data) > 1 {
            memData, err := strconv.Atoi(data[1])
            if err != nil {
                return -1, -1, -1, -1, err
            }
            memInfo[strings.TrimRight(data[0], ":")] = int32(memData)
        }
    }
    // Unit is KB
    return memInfo["MemTotal"], memInfo["MemAvailable"], memInfo["MemFree"], (memInfo["MemTotal"] - memInfo["MemAvailable"]), nil
}

func GetRootStorageInfo() (int32, int32, int32, error) {
    var stat syscall.Statfs_t
    err := syscall.Statfs("/", &stat)
    if err != nil {
        return -1, -1, -1, err
    }

    // Convert to MB
    totalSize := (stat.Blocks * uint64(stat.Bsize)) / 1000 / 1000
    availSize := (stat.Bavail * uint64(stat.Bsize)) / 1000 / 1000
    return int32(totalSize), int32(availSize), int32(totalSize - availSize), nil
}

func DisplayStat(prefix string, stat string, warning bool, suffix string) {
    fmt.Print(prefix, ":\t")
    if warning {
        ct.Foreground(ct.Red, false)
    }
    fmt.Print(stat)
    fmt.Print(suffix)
    ct.ResetColor()
}

func main() {
    numCores := runtime.NumCPU()

    secondsUp, secondsIdle, err := GetUpTime()
    if err != nil {
        log.Fatal(err)
    }
    durationUp := time.Duration(secondsUp) * time.Second

    oneMinuteLoad, fiveMinuteLoad, fifteenMinuteLoad, err := GetLoadAvg()
    if err != nil {
        log.Fatal(err)
    }

    cpuUsage, err := GetCPUUsage()
    if err != nil {
        log.Fatal(err)
    }

    cpuTemp, err := GetCPUTemp()
    if err != nil {
        log.Fatal(err)
    }

    memTotal, memAvailable, _, memUsed, err := GetMemInfo()
    if err != nil {
        log.Fatal(err)
    }
    memRatio := float32(memUsed) * 100 / float32(memTotal)

    rootTotal, rootAvailable, rootUsed, err := GetRootStorageInfo()
    if err != nil {
        log.Fatal(err)
    }
    rootRatio := float64(rootUsed) * 100 / float64(rootTotal)

    maxLoad := math.Max(oneMinuteLoad, math.Max(fiveMinuteLoad, fifteenMinuteLoad))
    twoTimesCores := float64(numCores) * 2
    DisplayStat(
        "System Load",
        fmt.Sprintf("%.2f %.2f %.2f", oneMinuteLoad, fiveMinuteLoad, fifteenMinuteLoad),
        maxLoad > twoTimesCores,
        "\t")
    DisplayStat(
        "Uptime",
        durationUp.String(),
        false,
        fmt.Sprintf(" (%.2f%% idle)\n", float32(secondsIdle) / float32(secondsUp)))

    DisplayStat("CPU Usage", fmt.Sprintf("%.2f", cpuUsage), cpuUsage > 90, "%\t\t")
    DisplayStat("CPU Temp", fmt.Sprintf("%.0f", cpuTemp), cpuTemp > 45, "°C\n")

    DisplayStat(
        "Memory Usage",
        fmt.Sprintf("%.2f", memRatio),
        memRatio > 95,
        fmt.Sprintf("%% of %dMB (%dMB Avail)\n", memTotal / 1000, memAvailable / 1000))
    DisplayStat(
        "Usage of /",
        fmt.Sprintf("%.2f", rootRatio),
        memRatio > 95,
        fmt.Sprintf("%% of %dGB (%dGB Avail)\n", rootTotal / 1000, rootAvailable / 1000))
}
