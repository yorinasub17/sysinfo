# Changelog

## v0.2.0

- Display system load average
- Display uptime
- Display root disk usage
- Reorganize table

## v0.1.1

Bugfix
- CPU info can exceed 32 bits


## v0.1.0

Initial release

- Display CPU usage as percentage (sys + user)
- Display CPU temperature in degrees C
- Display memory usage info as percentage
- Warning labels (CPU usage >90; CPU temp >45; Mem usage >95)
