# sysinfo

Simple binary that will display system information in a neat format. Best when
used in `/etc/update-motd.d/` to display stats on SSH login.
